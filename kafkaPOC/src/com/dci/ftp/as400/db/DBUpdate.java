package com.dci.ftp.as400.db;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.CallableStatement;
/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */
import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.apache.logging.log4j.EventLogger;

import com.dci.ftp.exception.StoredProcException;
import com.dci.ftp.logger.DCILogger;
import com.ibm.as400.access.AS400JDBCCallableStatement;

import kafka.consumerKafka;

/**
 * @@author Durgesh Created on Sep 3, 2007 This is the DBUpdate class
 */

public class DBUpdate {

	/**
	 * This is the static method
	 * 
	 * @@param library
	 * @@param clientId
	 * @@param ifsPath
	 * @@return returnValue
	 * @@throws StoredProcException
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	public static void callStoredProc(String library, String clientId, String ifsPath)
			throws StoredProcException, UnsupportedEncodingException, IOException {
		Connection conn = null;
		AS400JDBCCallableStatement proc = null;
		boolean returnValue = false;
		String ftpUploadString = "SFTP Upload";
	//	String callablString="";
		String callablString = "call " + library + ".SP_IMENQUEUEIMPORT(?,?,?,?,?)";
		String effectiveDate = null;
		String expiryDate = null;
		CallableStatement cs = null;
		System.out.println("Entered the SP call function.");
		
		try {

			conn = DBUtil.getInstance().getConnection();
			proc = (AS400JDBCCallableStatement) conn.prepareCall(callablString);
			System.out.print(callablString);

			proc.setString(1, ftpUploadString);
			proc.setInt(2, Integer.parseInt(clientId));
			proc.setString(3, ifsPath);
			proc.setString(4, null);
			proc.setString(5, null);
			
					//cs = conn.prepareCall(callablString);
		//	cs = conn.prepareCall("{call SP_IMENQUEUEIMPORT(?,?,?,?,?)}");
					/*cs.setString(1, ftpUploadString);
					cs.setInt(2, Integer.parseInt(clientId));
					cs.setString(3, ifsPath);
					cs.setDate(4,null);
					cs.setDate(5,null);*/
				
			System.out.println("Completed setting following parameters.");
			DCILogger.audit("Completed setting following parameters." + ftpUploadString + "," + clientId + "," + ifsPath
					+ "," + effectiveDate + "," + expiryDate);

			System.out.println("Parameters for the call set.");
			returnValue = proc.execute();
		//	cs.executeUpdate();
			System.out.println("SP call made, with return value: "+returnValue);
			DCILogger.audit("Stored Procedure executed successfully." + callablString);
			DCILogger.audit("library : " + library + "  clientId : " + clientId + "  ifsPath : " + ifsPath);
			/*if (returnValue) {
				DCILogger.audit("Stored Procedure executed successfully." + callablString);
				DCILogger.audit("library : " + library + "  clientId : " + clientId + "  ifsPath : " + ifsPath);

			} else {
				DCILogger.audit("Stored Procedure Returned False. " + callablString);
				DCILogger.audit("library : " + library + "  clientId : " + clientId + "  ifsPath : " + ifsPath);
			}*/

		}
		catch (Exception e) {
			String exceptionString1 = e.getMessage();
			DCILogger.debug(exceptionString1);
//			throw new StoredProcException(exceptionString1 + "\n command : " + callablString);
		} finally {
			try {
				if (proc != null)
					proc.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		
		System.out.println("Exiting Stored procedure...");
		/*return returnValue;*/
	}

}
