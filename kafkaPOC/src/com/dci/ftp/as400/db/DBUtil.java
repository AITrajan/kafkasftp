package com.dci.ftp.as400.db;

import java.io.FileReader;
/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;
import com.dci.ftp.logger.DCILogger;
import com.ibm.as400.access.AS400JDBCDataSource;

/**
 * @@author Durgesh Created on Sep 3, 2007 This is DBUtil class Window -
 *          Preferences - Java - Code Style - Code Templates
 */
public class DBUtil {
	private static AS400JDBCDataSource as400DataSource;
	private static DBUtil instance = null;

	protected DBUtil() throws Exception {

		try {
			registerDataSource();
		} catch (Exception e) {
			e.printStackTrace();
			DCILogger.debug(e.getMessage());
			throw e;
		}
	}

	private void registerDataSource() throws Exception {
		try {
			/*
			 * Properties bundle = new Properties(); FileReader reader = new
			 * FileReader("UtilProperties.properties"); bundle.load(reader);
			 */ResourceBundle bundle = ResourceBundle.getBundle("UtilProperties");

			String serverName = (String) bundle.getString("dbServerName");
			String password = (String) bundle.getString("dbPwd");

			String userID = (String) bundle.getString("dbUser");
			String dbName = (String) bundle.getString("dbName");

			// Create a data source for making the connection.
			as400DataSource = new AS400JDBCDataSource(serverName);
			as400DataSource.setUser(userID);
			as400DataSource.setPassword(password);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	public static DBUtil getInstance() throws Exception {
		if (instance == null) {
			instance = new DBUtil();
		}

		return instance;
	}

	public Connection getConnection() throws SQLException {
		// connection to DB2
		Connection connection = null;

		try {
			connection = as400DataSource.getConnection();

		} catch (SQLException e) {
			throw e;
		}

		return connection;
	}

	public void close(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			DCILogger.error(e.getMessage());
		}

	}

}
