package com.dci.ftp.as400.db;
/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.ibm.as400.access.AS400JDBCConnectionPool;
import com.ibm.as400.access.AS400JDBCConnectionPoolDataSource;
import com.ibm.as400.access.ConnectionPoolException;

/**
 * @@author Durgesh
 * Created on Sep 3, 2007
 * This is DBUtilPool class
 * 
 */
public class DBUtilPool {

	
   private static DBUtilPool instance = null;
   protected DBUtilPool() {
   
   		registerConnectionPoolDataSource();
   }
   public static DBUtilPool getInstance() {
      if(instance == null) {
         instance = new DBUtilPool();
      }
      return instance;
   }
   

   	private static InitialContext context = null;
	private final static String datasourceName = "jdbc/docloader";
	private static AS400JDBCConnectionPoolDataSource as400CPDataSource;
	private static AS400JDBCConnectionPool pool ;
	private static boolean dataSourceNone = false;

	/**
	 * @@return
	 * @@throws NamingException
	 */
	private static InitialContext getInitialContext() throws NamingException {

		if (context == null) {
			try {
				  Hashtable env = new Hashtable();
				  env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
				  context = new InitialContext(env);
				  
			} catch (NamingException ne) {
				ne.printStackTrace();
				throw ne;
			}
		}

		return context;
	}	

	/**
	 * @@return
	 * @@throws NamingException
	 */
	private static AS400JDBCConnectionPoolDataSource getDataSource() throws NamingException {

		if (as400CPDataSource == null) {
			InitialContext context = getInitialContext();
			as400CPDataSource = (AS400JDBCConnectionPoolDataSource) context.lookup(datasourceName);
		}
	
		return as400CPDataSource;

	} 	
		
	/**
	 * This is method for registerConnectionPoolDataSource
	 */
	private static void registerConnectionPoolDataSource() {
		
	  // Create a data source to the i5/OS database.
		AS400JDBCConnectionPoolDataSource connectionPoolDatasource = new AS400JDBCConnectionPoolDataSource();
		connectionPoolDatasource.setServerName("12.44.56.218");
		connectionPoolDatasource.setUser("ULTRAMATIC");
		connectionPoolDatasource.setPassword("ULTRAMAT1A");
		//connectionPoolDatasource.setDatabaseName("ATLAS");
		
		Context context=null;
		try {
			  // Register the datasource with the Java Naming and Directory Interface (JNDI).
			context= getInitialContext();
			context.rebind("jdbc/docloader", connectionPoolDatasource);
			 
		} catch (NamingException e1) {
			//  Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	/**
	 * @@return connection
	 */
	public Connection getPoolConnection() {
		
		AS400JDBCConnectionPoolDataSource connnectionPoolDatasource =null;;
		Context context = null;
		Connection connection = null;
		try {
			
			connnectionPoolDatasource = getDataSource();
			
			if (pool == null) {
				// Create an AS400JDBCConnectionPool object.
				pool = new AS400JDBCConnectionPool(connnectionPoolDatasource);
			}
			
			if(pool.getAvailableConnectionCount()==0){
				//	Adds 2 connections to the pool that can be used by the application (creates the physical database connections based on the data source).
			   // pool.fill(2);
				//connection = pool.getConnection();
			}else{
			  	connection = pool.getConnection();
			}
			
		} catch (NamingException e2) {
			//  Auto-generated catch block
			e2.printStackTrace();
		} catch (ConnectionPoolException e) {
			//  Auto-generated catch block
			e.printStackTrace();
		}

		return connection;
	}

	public void close(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			//  Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}