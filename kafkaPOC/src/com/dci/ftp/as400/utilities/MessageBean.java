package com.dci.ftp.as400.utilities;
/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */

/**
 * @@author Durgesh
 * Created on Sep 3, 2007
 * This is MessageBean class
 * 
 */
public class MessageBean {
	
	public String cliendID;
	public String filePath;
	public long messageTimeInMillis;
	public byte[] messageKey;
	
	/**
	 * @@return Returns the cliendID.
	 */
	public String getCliendID() {
		return cliendID;
	}
	/**
	 * @@param cliendID The cliendID to set.
	 */
	public void setCliendID(String cliendID) {
		this.cliendID = cliendID;
	}
	/**
	 * @@return Returns the filePath.
	 */
	public String getFilePath() {
		return filePath;
	}
	/**
	 * @@param filePath The filePath to set.
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	/**
	 * @@return Returns the messageKey.
	 */
	public byte[] getMessageKey() {
		return messageKey;
	}
	/**
	 * @@param messageKey The messageKey to set.
	 */
	public void setMessageKey(byte[] messageKey) {
		this.messageKey = messageKey;
	}
	/**
	 * @@return Returns the messageTime.
	 */
	public long getMessageTimeInMillis() {
		return messageTimeInMillis;
	}
	/**
	 * @@param messageTime The messageTime to set.
	 */
	public void setMessageTime(long messageTimeInMillis) {
		this.messageTimeInMillis = messageTimeInMillis;
	}
	
}
