package com.dci.ftp.as400.utilities;

/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.StringTokenizer;

import org.apache.log4j.BasicConfigurator;

import com.dci.ftp.exception.FileMoveException;
import com.dci.ftp.exception.ReadMessageException;
import com.dci.ftp.exception.RemoveMessageException;
import com.dci.ftp.exception.SubmitCMDException;
import com.dci.ftp.logger.DCILogger;
import com.dci.ftp.xml.BaseBean;
import com.dci.ftp.xml.ClientBean;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.MessageQueue;
import com.ibm.as400.access.ObjectDoesNotExistException;
import com.ibm.as400.access.QSYSObjectPathName;
import com.ibm.as400.access.QueuedMessage;

/**
 * @@author Durgesh Created on Sep 11, 2007 This is ConnectAS400 class
 */

public class ConnectAS400 {

	/**
	 * @@param systemName
	 * @@param userID
	 * @@param password
	 * @@return
	 */
	public AS400 connect(String systemName, String userID, String password) {

		AS400 system = new AS400(systemName, userID, password);
		return system;

	}

	/**
	 * @@param system
	 */
	public void disconnect(AS400 system) {
		system.disconnectAllServices();
	}

	/**
	 * @@param system
	 * @@param fromDir
	 * @@param toDir
	 * @@return
	 * @@throws FileMoveException
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	public String renameDir(AS400 system, String fromDir, String toDir, String watchFolder)
			throws FileMoveException, UnsupportedEncodingException, IOException {

		String newIFSFile = "";
		boolean fileRenamed = false;

		DCILogger.audit("Inside rename function");
		try {

			IFSFile fromFile = new IFSFile(system, fromDir);
			String messageFileName = fromFile.getName();
			// filename without the extension
			String messageFilenameNoExt;

			// extension without the dot
			String messageFileExt;

			// where the last dot is. There may be more than one.
			int dot = messageFileName.lastIndexOf('.');

			if (dot >= 0) {

				messageFilenameNoExt = messageFileName.substring(0, dot);
				messageFileExt = messageFileName.substring(dot + 1);

			} else {
				// was no extension
				messageFilenameNoExt = messageFileName;
				messageFileExt = "";
			}
			File file = null;

//			if (watchFolder.equalsIgnoreCase("."))
//				file = new File(System.getProperty("user.dir") + toDir);
//			else if (watchFolder.length() >= 2) {
//				file = new File(watchFolder + toDir);
//			} else {
			file = new File(toDir);
//			}
			IFSFile toFile = new IFSFile(system, file.getAbsolutePath());
			String newDir = toFile.getAbsolutePath();
			newDir = newDir.replace('\\', '/');

			Date date = new Date(System.currentTimeMillis());

			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmSS");
			String dateString = sdf.format(date);
			StringBuffer newFileName = new StringBuffer(newDir);
			newFileName.append("/");
			newFileName.append(messageFilenameNoExt);
			newFileName.append("_");
			newFileName.append(dateString);

			if (dot >= 0) {
				newFileName.append(".");
				newFileName.append(messageFileExt);
			}

			IFSFile toNewFile = new IFSFile(system, newFileName.toString());
			File afile = new File(fromFile.getAbsolutePath());

			if (afile.renameTo(new File(toNewFile.getAbsolutePath()))) {
				System.out.println("File is moved successful!");
				newIFSFile = newFileName.toString();
			} else {
				System.out.println("File is failed to move!");
			}

		} catch (Exception pe) {
			DCILogger.audit("Exception occured:" + pe.getLocalizedMessage());
			pe.printStackTrace();
			String exceptionString1 = pe.getMessage();
			throw new FileMoveException(exceptionString1);
		}
		return newIFSFile;
	}

	/**
	 * @@param system
	 * @@param messageKey
	 * @@param libraryName
	 * @@param objectName
	 * @@param objectType
	 * @@throws RemoveMessageException
	 */
	public void removeMessage(AS400 system, byte[] messageKey, String libraryName, String objectName, String objectType)
			throws RemoveMessageException {

		QSYSObjectPathName qsysPath = null;
		MessageQueue queue = null;

		try {

			qsysPath = new QSYSObjectPathName(libraryName, objectName, objectType);
			queue = new MessageQueue(system, qsysPath.getPath());
			queue.remove(messageKey);

		} catch (AS400SecurityException e) {
			e.printStackTrace();
			String exception = " Failed to remove message. messageKey: " + messageKey + "/n" + e.getMessage();
//        	//DCILogger.debug(exception);
			throw new RemoveMessageException(exception);
		} catch (ErrorCompletingRequestException e) {
			e.printStackTrace();
			String exception = " Failed to remove message. messageKey: " + messageKey + "/n" + e.getMessage();
//        	//DCILogger.debug(exception);
			throw new RemoveMessageException(exception);
		} catch (InterruptedException e) {
			e.printStackTrace();
			String exception = " Failed to remove message. messageKey: " + messageKey + "/n" + e.getMessage();
//        	//DCILogger.debug(exception);
			throw new RemoveMessageException(exception);
		} catch (IOException e) {
			e.printStackTrace();
			String exception = " Failed to remove message. messageKey: " + messageKey + "/n" + e.getMessage();
//        	//DCILogger.debug(exception);
			throw new RemoveMessageException(exception);
		} catch (ObjectDoesNotExistException e) {
			e.printStackTrace();
			String exception = " Failed to remove message. messageKey: " + messageKey + "/n" + e.getMessage();
//        	//DCILogger.debug(exception);
			throw new RemoveMessageException(exception);
		}
	}

	/**
	 * @@param system
	 * @@param libraryName
	 * @@param objectName
	 * @@param objectType
	 * @@return
	 * @@throws ReadMessageException
	 */
	public ArrayList readMessage(AS400 system, String libraryName, String objectName, String objectType)
			throws ReadMessageException {

		QSYSObjectPathName qsysPath = null;
		MessageQueue queue = null;
		Enumeration messages = null;
		ArrayList messageList = null;

		try {

			qsysPath = new QSYSObjectPathName(libraryName, objectName, objectType);

			queue = new MessageQueue(system, qsysPath.getPath());

			messages = queue.getMessages();
			QueuedMessage qMsg = null;

			String message = null;
			String clientID = null;
			String filePath = null;
			messageList = new ArrayList();
			MessageBean messageBean = null;

			while (messages.hasMoreElements()) {

				messageBean = new MessageBean();
				qMsg = (QueuedMessage) messages.nextElement();
				message = qMsg.getText();
//				 //DCILogger.audit("ConnectAS400.readMessage message :: "+message);

				StringTokenizer st = new StringTokenizer(message, ",");
				if (st.hasMoreTokens()) {
					clientID = st.nextToken();
					filePath = st.nextToken();
				}

				messageBean.setCliendID(clientID);
				messageBean.setFilePath(filePath);
				messageBean.setMessageKey(qMsg.getKey());
				messageBean.setMessageTime(qMsg.getDate().getTimeInMillis());
				messageList.add(messageBean);

			}

		} catch (AS400SecurityException e) {
			e.printStackTrace();
			String exception = " Failed to read messages from the queue. ./n" + e.getMessage();
			// DCILogger.debug(exception);
			throw new ReadMessageException(exception);
		} catch (ErrorCompletingRequestException e) {
			e.printStackTrace();
			String exception = " Failed to read messages from the queue. ./n" + e.getMessage();
			// DCILogger.debug(exception);
			throw new ReadMessageException(exception);
		} catch (InterruptedException e) {
			e.printStackTrace();
			String exception = " Failed to read messages from the queue. ./n" + e.getMessage();
			// DCILogger.debug(exception);
			throw new ReadMessageException(exception);
		} catch (IOException e) {
			e.printStackTrace();
			String exception = " Failed to read messages from the queue. ./n" + e.getMessage();
			// DCILogger.debug(exception);
			throw new ReadMessageException(exception);
		} catch (ObjectDoesNotExistException e) {
			e.printStackTrace();
			String exception = " Failed to read messages from the queue. ./n" + e.getMessage();
			// DCILogger.debug(exception);
			throw new ReadMessageException(exception);
		}

		return messageList;
	}

	/**
	 * @@param messageTimeInMillis
	 * @@param maxTimeToLive
	 * @@return
	 */
	public boolean isMessageValid(long messageTimeInMillis, long maxTimeToLive) {

		// read the maxTimeToLive from properties file.
		// long maxTimeToLive= 30*60*60*1000;

		long currentTimeInMillis = Calendar.getInstance().getTimeInMillis();
		long elapsedTime = messageTimeInMillis - currentTimeInMillis;
		if (elapsedTime < maxTimeToLive) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * @@param system
	 * @@param fileName
	 * @@param watchFolder
	 * @@param propsABSPath
	 * @@return
	 */
	public boolean isFileInWatchFolder(AS400 system, String fileName, String watchFolder, String propsABSPath) {

		IFSFile fromFile = new IFSFile(system, fileName);
		String systemABSPath = fromFile.getAbsolutePath();
		String messageFileName = fromFile.getName();

		// verify the watch folder property.
		boolean returnValue = false;
		if (watchFolder.equalsIgnoreCase(".")) {
			String propsStringPath = "/" + propsABSPath + "/" + messageFileName;
			if (propsStringPath.equalsIgnoreCase(systemABSPath))
				returnValue = true;
		} else {
			String propsStringPath = propsABSPath + "/" + watchFolder + "/" + messageFileName;
			if (propsStringPath.equalsIgnoreCase(systemABSPath))
				// returnValue=fileName.indexOf(watchFolder) > 0;
				returnValue = true;
		}
		return returnValue;
	}

	/**
	 * @@param system
	 * @@param ftpUploadID
	 * @@param classpath
	 * @@param jobq
	 * @@param jobd
	 * @@param user
	 * @@return
	 * @@throws SubmitCMDException
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	public boolean submitAS400CommandJob(AS400 system, String ftpUploadID, String classpath, String jobq, String jobd,
			String user) throws SubmitCMDException, UnsupportedEncodingException, IOException {

		String commandString = "SBMJOB  CMD(JAVA CLASS(com.CSVApp) PARM('FTP Upload' " + ftpUploadID + ") CLASSPATH("
				+ classpath + "))  JOB(" + user + ")  JOBD(" + jobd + ")  JOBQ(" + jobq + ")  USER(" + user
				+ ")  LOG(4 0 *SECLVL)  CPYENVVAR(*YES)";
		DCILogger.debug("ConnectAS400.submitAS400CommandJob commandString : " + commandString);

		CommandCall command = new CommandCall(system);
		boolean commandResult = false;
		try {
			// Run the command SBMJOB
			commandResult = command.run(commandString);
			System.out.println("CommandCAll has been executed.");
			if (commandResult != true) {
				// Note that there was an error.
				DCILogger.audit("ConnectAS400.submitAS400CommandJob : Command execution failed");
				DCILogger.debug("ConnectAS400.submitAS400CommandJob : Command execution failed");
				// Audit Log : command execution failed.
			} else {
				// Audit Log : command executed sucessfully.
				DCILogger.audit("ConnectAS400.submitAS400CommandJob : Command executed sucessfully");
				DCILogger.debug("ConnectAS400.submitAS400CommandJob : Command executed sucessfully");
			}
			// Show the messages (returned whether or not there was an error.)
			AS400Message[] messagelist = command.getMessageList();
			for (int i = 0; i < messagelist.length; ++i) {
				// Show each message.
				DCILogger.audit(messagelist[i].getText());
				DCILogger.debug(messagelist[i].getText());
			}
			if (commandResult == false) {
				String exception = "ConnectAS400.submitAS400CommandJob : Failed to Execute the submit command. "
						+ commandString;
				DCILogger.audit("Command result is false");
				throw new SubmitCMDException(exception);
			}

		} catch (AS400SecurityException e) {
			DCILogger.audit("Command " + command.getCommand() + " issued an exception!");
			e.printStackTrace();
			String exception = "Failed to Execute the submit command. " + commandString + "./n" + e.getMessage();
			DCILogger.debug(exception);
			throw new SubmitCMDException(exception);

		} catch (ErrorCompletingRequestException e) {
			DCILogger.audit("Command " + command.getCommand() + " issued an exception!");
			e.printStackTrace();
			String exception = " Failed to Execute the submit command. " + commandString + "./n" + e.getMessage();
			DCILogger.debug(exception);
			throw new SubmitCMDException(exception);
		} catch (IOException e) {
			DCILogger.audit("Command " + command.getCommand() + " issued an exception!");
			e.printStackTrace();
			String exception = " Failed to Execute the submit command. " + commandString + "./n" + e.getMessage();
			DCILogger.debug(exception);
			throw new SubmitCMDException(exception);
		} catch (InterruptedException e) {
			DCILogger.audit("Command " + command.getCommand() + " issued an exception!");
			e.printStackTrace();
			String exception = " Failed to Execute the submit command. " + commandString + "./n" + e.getMessage();
			DCILogger.debug(exception);
			throw new SubmitCMDException(exception);
		} catch (PropertyVetoException e) {
			DCILogger.audit("Command " + command.getCommand() + " issued an exception!");
			e.printStackTrace();
			String exception = " Failed to Execute the submit command. " + commandString + "./n" + e.getMessage();
			DCILogger.debug(exception);
			throw new SubmitCMDException(exception);
		}
		// Done with the system.
		system.disconnectService(AS400.COMMAND);

		return commandResult;
	}

//	public void submitUploadJob(String user, String client ,String Job_Name,String Job_Description,String Job_Queue , String userRes,String log,String cpyEnvVar,String jobPriority,String class_path) throws Exception{
	public boolean submitUploadJob(ClientBean clientBean) throws Exception {

		AS400 system = null;
		CommandCall command = null;
		boolean commandResult = false;

		try {
			BasicConfigurator.configure();
			/* String server = InetAddress.getLocalHost().getHostName(); */
			String server = "10.162.205.99";
			if (server != null && server.indexOf("mis") > -1)
				server = "phoenix";
			DCILogger.audit("Job Submitting to     :  " + server);
			DCILogger.debug("Job Submitting to     :  " + server);
			system = new AS400(server, clientBean.user, clientBean.password);
			command = new CommandCall(system);
			/*
			 * String iSeriesCommand =
			 * 
			 * "SBMJOB  CMD(JAVA CLASS(com.CSVApp) PARM('FTP Upload' " + ftpUploadID +
			 * ") CLASSPATH(" + classpath + "))  JOB(" + user + ")  JOBD(" + jobd +
			 * ")  JOBQ(" + jobq + ")  USER(" + user +
			 * ")  LOG(4 0 *SECLVL)  CPYENVVAR(*YES)";
			 */
			CommandJob cmdJob = new CommandJob();

			String iSeriesCommand = cmdJob.getSubmitJob(
					"JAVA CLASS(com.CSVApp) PARM('" + clientBean.userId + "' " + clientBean.dbClientId + ") "
							+ clientBean.class_path,
					clientBean.Job_Name, clientBean.Job_Description, clientBean.Job_Queue, clientBean.user,
					clientBean.log, clientBean.cpyEnvVar, null);
			// Run the command "send message."
			DCILogger.debug(iSeriesCommand);
			DCILogger.audit(iSeriesCommand);
			commandResult = command.run(iSeriesCommand);

			if (!commandResult) {
				// Note that there was an error.
				AS400Message[] messagelist = command.getMessageList();
				for (int i = 0; i < messagelist.length; ++i) {
					// Show each message.
					DCILogger.debug(messagelist[i].getText());
					DCILogger.audit(messagelist[i].getText());
				}
				throw new Exception("Command failed to execute.");
			}
			// Show the messages (returned whether or not there was an error.)
			DCILogger.debug("End of commands");
			DCILogger.audit("End of commands");
		} catch (Exception e) {
//				new DCIException("com.dci.db4.utils.CommandJob.submitUploadJob()", e);
			DCILogger.audit(e.getLocalizedMessage());
			if (command != null) {
				DCILogger.audit("Command " + command.getCommand() + " issued an exception!");
				DCILogger.debug("Command " + command.getCommand() + " issued an exception!");
			}
			e.printStackTrace();
		} finally {
			if (system != null)
				system.disconnectService(AS400.COMMAND);
		}

		return commandResult;
	}

	/**
	 * @@param system
	 * @@param emailShortMsg
	 * @@param emailLongMsg
	 * @@param emailId
	 * @@return
	 * @@throws SubmitCMDException
	 */
	public boolean sendAS400Email(AS400 system, String emailShortMsg, String emailLongMsg, String emailId)
			throws SubmitCMDException {

		String commandString = "SNDDST  TYPE(*LMSG) TOINTNET((*NONE) ('" + emailId + "')) DSTD('" + emailShortMsg
				+ "')  LONGMSG('" + emailLongMsg + "') DOCD(*DSTD) SUBJECT(*DOCD)";
		CommandCall command = new CommandCall(system);
		boolean commandResult = false;
		try {
			// Run the command SBMJOB
			commandResult = command.run(commandString);
			System.out.println("Command executed and returned: " + commandResult);
			if (commandResult != true) {
				// Note that there was an error.
				DCILogger.audit("ConnectAS400.sendAS400Email : Command execution failed");
				System.out.println("Command failed to execute.");
				// Audit Log : command execution failed.
			} else {
				// Audit Log : command executed sucessfully.
				DCILogger.audit("ConnectAS400.sendAS400Email : " + commandString + " executed sucessfully");
				System.out.println("Command executed successfully.");
			}
			// Show the messages (returned whether or not there was an error.)
			AS400Message[] messagelist = command.getMessageList();
			for (int i = 0; i < messagelist.length; ++i) {
				// Show each message.
				DCILogger.audit(messagelist[i].getText());

			}
			if (commandResult == false) {
				String exception = "ConnectAS400.sendAS400Email Failed to Execute the submit command. " + commandString;
				throw new SubmitCMDException(exception);
			}

		} catch (AS400SecurityException e) {
			DCILogger.audit("Command " + command.getCommand() + " issued an exception!");
			e.printStackTrace();
			String exception = " Failed to Execute the submit command. " + commandString + "./n" + e.getMessage();
			DCILogger.debug(exception);
			throw new SubmitCMDException(exception);

		} catch (ErrorCompletingRequestException e) {
			DCILogger.audit("Command " + command.getCommand() + " issued an exception!");
			e.printStackTrace();
			String exception = " Failed to Execute the submit command. " + commandString + "./n" + e.getMessage();
			DCILogger.debug(exception);
			throw new SubmitCMDException(exception);
		} catch (IOException e) {
			DCILogger.audit("Command " + command.getCommand() + " issued an exception!");
			e.printStackTrace();
			String exception = " Failed to Execute the submit command. " + commandString + "./n" + e.getMessage();
			DCILogger.debug(exception);
			throw new SubmitCMDException(exception);
		} catch (InterruptedException e) {
			DCILogger.audit("Command " + command.getCommand() + " issued an exception!");
			e.printStackTrace();
			String exception = " Failed to Execute the submit command. " + commandString + "./n" + e.getMessage();
			DCILogger.debug(exception);
			throw new SubmitCMDException(exception);
		} catch (PropertyVetoException e) {
			DCILogger.audit("Command " + command.getCommand() + " issued an exception!");
			e.printStackTrace();
			String exception = " Failed to Execute the submit command. " + commandString + "./n" + e.getMessage();
			DCILogger.debug(exception);
			throw new SubmitCMDException(exception);
		}
		// Done with the system.
		system.disconnectService(AS400.COMMAND);
		System.out.println("Disconnected from the systems post AS400 execution");

		return commandResult;
	}

}
