package com.dci.ftp.as400.utilities;

/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 *//*
	* Created on Nov 3, 2004
	*
	* To change the template for this generated file go to
	* Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
	*/
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;

import java.net.*;

import com.dci.ftp.logger.DCILogger;
import com.dci.ftp.xml.BaseBean;

import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

public class CommandJob {
	Logger consume = Logger.getLogger(CommandJob.class);

	/**
	 * @Purpose: This method is used to run Make book processing job.
	 * @return void
	 * @throws Exception TODO
	 */

	public void submitJob() throws Exception {
		AS400 system = null;
		CommandCall command = null;
		BaseBean baseBean = new BaseBean();
		try {
			BasicConfigurator.configure();
			String server = InetAddress.getLocalHost().getHostName();
			if (server != null && server.indexOf("mis") > -1)
				server = "phoenix";
			// server="192.168.200.3";
			consume.debug("Job Submitting to     :  " + server);
			system = new AS400(server, baseBean.user, baseBean.password);
			command = new CommandCall(system);
			if (command.run("CALL ZDBMON2") != true) {
				// Note that there was an error.
				consume.error("Command failed!");
				throw new Exception("Command failed to execute.");
			}
			// Show the messages (returned whether or not there was an error.)
			AS400Message[] messagelist = command.getMessageList();
			for (int i = 0; i < messagelist.length; ++i) {
				// Show each message.
				consume.debug(messagelist[i].getText());
			}

			consume.debug("End of commands");
		} catch (Exception e) {
//				new DCIException("com.dci.db4.utils.CommandJob.submitJob()", e);
			DCILogger.audit(e.getLocalizedMessage());
			e.printStackTrace();
			if (command != null)
				consume.error("Command " + command.getCommand() + " issued an exception!");
			throw e;
		} finally {
			if (system != null)
				system.disconnectService(AS400.COMMAND);
		}
	}

	/**
	 * @Purpose: This method is used to submit Upload Job
	 * @return void
	 * @throws Exception TODO
	 */
	public void submitUploadJob(String user, String client, String Job_Name, String Job_Description, String Job_Queue,
			String userRes, String log, String cpyEnvVar, String jobPriority, String class_path) throws Exception {
		AS400 system = null;
		CommandCall command = null;
		BaseBean baseBean = new BaseBean();
		try {
			BasicConfigurator.configure();
			String server = InetAddress.getLocalHost().getHostName();
			if (server != null && server.indexOf("mis") > -1)
				server = "phoenix";
			consume.debug("Job Submitting to     :  " + server);
			system = new AS400(server, baseBean.user, baseBean.password);
			command = new CommandCall(system);

			String iSeriesCommand = getSubmitJob(
					"JAVA CLASS(com.CSVApp) PARM('" + user + "' " + client + ") " + class_path, Job_Name,
					Job_Description, Job_Queue, userRes, log, cpyEnvVar, null);
			// Run the command "send message."
			consume.debug(iSeriesCommand);
			if (command.run(iSeriesCommand) != true) {
				// Note that there was an error.
				AS400Message[] messagelist = command.getMessageList();
				for (int i = 0; i < messagelist.length; ++i) {
					// Show each message.
					consume.debug(messagelist[i].getText());
				}
				throw new Exception("Command failed to execute.");
			}
			// Show the messages (returned whether or not there was an error.)

			consume.debug("End of commands");
		} catch (Exception e) {
//				new DCIException("com.dci.db4.utils.CommandJob.submitUploadJob()", e);
			DCILogger.audit(e.getLocalizedMessage());
			if (command != null)
				consume.error("Command " + command.getCommand() + " issued an exception!");
			throw e;
		} finally {
			if (system != null)
				system.disconnectService(AS400.COMMAND);
		}
	}

	/**
	 * @Purpose: This method is used to get SubmitJob
	 * @return void TODO
	 */

	public String getSubmitJob(String cmd, String jobName, String jobDescription, String jobQueue, String user,
			String log, String cpyEnvVar, String jobPriority) {

		if (cmd == null || cmd.length() == 0 || jobName == null || jobName.length() == 0)
			return null;
		StringBuffer sb = new StringBuffer("SBMJOB ");
		sb.append(" CMD(" + cmd + ") ");

		if (jobName != null)
			sb.append(" JOB(" + jobName + ") ");

		if (jobDescription != null)
			sb.append(" JOBD(" + jobDescription + ") ");

		if (jobQueue != null)
			sb.append(" JOBQ(" + jobQueue + ") ");

		if (jobPriority != null)
			sb.append(" JOBPTY(" + jobPriority + ") ");

		if (user != null)
			sb.append(" USER(" + user + ") ");

		if (log != null)
			sb.append(" LOG(" + log + ") ");

		if (cpyEnvVar != null)
			sb.append(" CPYENVVAR(" + cpyEnvVar + ") ");

		return sb.toString();
	}

	public static void main(String args[]) {
		try {
			new CommandJob().submitUploadJob("shivaji", "5", "Test1", "Test1", "Test1", "Test1", "Test1", "Test1",
					"Test1", "Test1");
		} catch (Exception e) {
//			new DCIException("com.dci.db4.utils.CommandJob.main()", e);
			DCILogger.audit(e.getLocalizedMessage());
		}
	}
}