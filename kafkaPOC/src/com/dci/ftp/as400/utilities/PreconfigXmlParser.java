package com.dci.ftp.as400.utilities;

/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */

/**
 * @project_name  dci
 * @package_name  com.dci.db4.utils
 * @class_name    PreconfigXmlParser.java : 3:48:18 PM
 * @creation_date June, 2008
 * @class_action  This class is to parse pre-configurations.xml
 * @creator      Rajasekhar and karan
* @author  Data Communique International
 * @version 1.0
 * @revision: <date Created>; Created
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
//import com.dci.db4.interceptor.ActionLogger;
import com.dci.ftp.logger.DCILogger;
import com.dci.ftp.xml.*;

public class PreconfigXmlParser extends DefaultHandler {

	HashMap client;
	
	HashMap property;
    
	private String tempClientId="";
	
	private String elementVal;

	private BaseBean bb;
	
//	private Logger developerLog = Logger.getLogger(ActionLogger.class);  
	  
	private String filePath= null;
	
	/**
	 * @Purpose: this method will get filePath
	 * @return String
	 */
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	public PreconfigXmlParser() {
		client = new HashMap();
	}

	/**
	 * @Purpose: this method will get getDepartmentConfigDetails
	 * @return HashMap
	 */
	public HashMap getDepartmentConfigDetails(ArrayList xmlPaths) throws SAXException, IOException, ParserConfigurationException {
		
     if(xmlPaths.size()!=0){		  
		  for(int i=0;i<xmlPaths.size();i++)
			{
			  setFilePath(xmlPaths.get(i).toString());
			  parseDocument();
			}
	}else{
		throw new IOException();
	} 
		return client;
	}


	/**
	 * @Purpose: this method is used to parseDocument
	 * @return void.
	 * @throws Exception
	 */
	private void parseDocument() throws SAXException, IOException, ParserConfigurationException {

		MDC.put("category","com.dci.db4.utils.PreconfigXmlParser");
		DCILogger.debug("Entering into com.dci.db4.utils.PreconfigXmlParser || Method Name : parseDocument() ||");
		DCILogger.debug(" ======================================================= ");
		DCILogger.debug("Retrieving information from "+getFilePath());
		DCILogger.debug(" ======================================================= ");
		// get a factory
		SAXParserFactory spf = SAXParserFactory.newInstance();
		// get a new instance of parser
		SAXParser sp = spf.newSAXParser();

		// parse the file and also register this class for call backs
		sp.parse(filePath, this);
	}

	
	/**
	 * @Purpose: Event Handlers
	 * @return void.
	 * @throws Exception
	 */
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// reset
		elementVal = "";
		if (qName.equalsIgnoreCase("client")) {
			property=new HashMap();
			tempClientId=attributes.getValue("id");
            property.clear();
		}
		 else if (qName.equalsIgnoreCase("property")) {
//			    bb = new PreConfigBean();
//			    bb.setPropertyName(attributes.getValue("name"));
//				bb.setPropertyValue(attributes.getValue("value"));
//				pcb.setPropertyLabel(attributes.getValue("label"));
			}
	}

	/**
	 * @Purpose: Event Handlers
	 * @return void.
	 * @throws Exception
	 */
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		elementVal = new String(ch, start, length);
	}

	/**
	 * @Purpose: Event Handlers
	 * @return void.
	 * @throws Exception
	 */
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (qName.equalsIgnoreCase("property")) {
//			property.put(pcb.getPropertyName(), pcb);
		}
		if (qName.equalsIgnoreCase("client")) {
//			pcb.setClientId(tempClientId);
//			client.put(pcb.getClientId(),property);
		}
	}
}