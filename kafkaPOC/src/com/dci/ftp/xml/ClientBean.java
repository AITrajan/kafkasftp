package com.dci.ftp.xml;
/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */

/**
 * @@author Durgesh
 * Created on Sep 7, 2007
 * This is ClientBean class
 */
public class ClientBean {
	public String userId = null;
	public String ftpSite = null;
	public String ftpId = null;
	public String ftpPass = null;
	public String ifsPath = null;
	public String library = null;
	public String classpath = null;
	public String jobd = null;
	public String jobq = null;
	public String user = null;
	public String email = null;
	public String watchFolder = null;
	public String absPath = null;
	public String clientName;
	public String clientId;
	public String dbClientId;
	public String client = null;
	public String password = null;
	public String Job_Name = null;
	public String Job_Queue = null;
	public String Job_Description = null;
	public String userRes = null;
	public String log = null;
	public String cpyEnvVar = null;
	public String jobPriority;
	public String class_path;
	

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @@return Returns the clientId.
	 */
	public String getClientId() {
		return clientId;
	}
	/**
	 * @@param clientId The clientId to set.
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	/**
	 * @@return Returns the clientName.
	 */
	public String getClientName() {
		return clientName;
	}
	/**
	 * @@param clientName The clientName to set.
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getJob_Name() {
		return Job_Name;
	}
	public void setJob_Name(String job_Name) {
		Job_Name = job_Name;
	}
	public String getJob_Queue() {
		return Job_Queue;
	}
	public void setJob_Queue(String job_Queue) {
		Job_Queue = job_Queue;
	}
	public String getJob_Description() {
		return Job_Description;
	}
	public void setJob_Description(String job_Description) {
		Job_Description = job_Description;
	}
	public String getUserRes() {
		return userRes;
	}
	public void setUserRes(String userRes) {
		this.userRes = userRes;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	public String getCpyEnvVar() {
		return cpyEnvVar;
	}
	public void setCpyEnvVar(String cpyEnvVar) {
		this.cpyEnvVar = cpyEnvVar;
	}
	public String getJobPriority() {
		return jobPriority;
	}
	public void setJobPriority(String jobPriority) {
		this.jobPriority = jobPriority;
	}
	public String getClass_path() {
		return class_path;
	}
	public void setClass_path(String class_path) {
		this.class_path = class_path;
	}
	/**
	 * @@return Returns the absPath.
	 */
	public String getAbsPath() {
		return absPath;
	}
	/**
	 * @@param absPath The absPath to set.
	 */
	public void setAbsPath(String absPath) {
		this.absPath = absPath;
	}
	/**
	 * @@return Returns the classpath.
	 */
	public String getClasspath() {
		return classpath;
	}
	/**
	 * @@param classpath The classpath to set.
	 */
	public void setClasspath(String classpath) {
		this.classpath = classpath;
	}
	/**
	 * @@return Returns the com.dci.ftp.email.
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @@param com.dci.ftp.email The com.dci.ftp.email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @@return Returns the ftpId.
	 */
	public String getFtpId() {
		return ftpId;
	}
	/**
	 * @@param ftpId The ftpId to set.
	 */
	public void setFtpId(String ftpId) {
		this.ftpId = ftpId;
	}
	/**
	 * @@return Returns the ftpPass.
	 */
	public String getFtpPass() {
		return ftpPass;
	}
	/**
	 * @@param ftpPass The ftpPass to set.
	 */
	public void setFtpPass(String ftpPass) {
		this.ftpPass = ftpPass;
	}
	/**
	 * @@return Returns the ftpSite.
	 */
	public String getFtpSite() {
		return ftpSite;
	}
	/**
	 * @@param ftpSite The ftpSite to set.
	 */
	public void setFtpSite(String ftpSite) {
		this.ftpSite = ftpSite;
	}
	/**
	 * @@return Returns the ifsPath.
	 */
	public String getIfsPath() {
		return ifsPath;
	}
	/**
	 * @@param ifsPath The ifsPath to set.
	 */
	public void setIfsPath(String ifsPath) {
		this.ifsPath = ifsPath;
	}
	/**
	 * @@return Returns the jobd.
	 */
	public String getJobd() {
		return jobd;
	}
	/**
	 * @@param jobd The jobd to set.
	 */
	public void setJobd(String jobd) {
		this.jobd = jobd;
	}
	/**
	 * @@return Returns the jobq.
	 */
	public String getJobq() {
		return jobq;
	}
	/**
	 * @@param jobq The jobq to set.
	 */
	public void setJobq(String jobq) {
		this.jobq = jobq;
	}
	/**
	 * @@return Returns the library.
	 */
	public String getLibrary() {
		return library;
	}
	/**
	 * @@param library The library to set.
	 */
	public void setLibrary(String library) {
		this.library = library;
	}
	/**
	 * @@return Returns the user.
	 */
	public String getUser() {
		return user;
	}
	/**
	 * @@param user The user to set.
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
	 * @@return Returns the watchFolder.
	 */
	public String getWatchFolder() {
		return watchFolder;
	}
	/**
	 * @@param watchFolder The watchFolder to set.
	 */
	public void setWatchFolder(String watchFolder) {
		this.watchFolder = watchFolder;
	}
	
	/**
	 * @@return Returns the dbClientId.
	 */
	public String getDbClientId() {
		return dbClientId;
	}
	/**
	 * @@param dbClientId The dbClientId to set.
	 */
	public void setDbClientId(String dbClientId) {
		this.dbClientId = dbClientId;
	}
}