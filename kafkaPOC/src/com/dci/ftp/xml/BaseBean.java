package com.dci.ftp.xml;

public class BaseBean {
	
	public String user = null;
	public String client = null;
	public String password = null;
	public String Job_Name = null;
	public String Job_Queue = null;
	public String Job_Description = null;
	public String userRes = null;
	public String log = null;
	public String cpyEnvVar = null;
	public String jobPriority;
	public String class_path;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFtpSite() {
		return ftpSite;
	}

	public void setFtpSite(String ftpSite) {
		this.ftpSite = ftpSite;
	}
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getJob_Name() {
		return Job_Name;
	}

	public void setJob_Name(String job_Name) {
		Job_Name = job_Name;
	}

	public String getJob_Queue() {
		return Job_Queue;
	}

	public void setJob_Queue(String job_Queue) {
		Job_Queue = job_Queue;
	}

	public String getJob_Description() {
		return Job_Description;
	}

	public void setJob_Description(String job_Description) {
		Job_Description = job_Description;
	}

	public String getUserRes() {
		return userRes;
	}

	public void setUserRes(String userRes) {
		this.userRes = userRes;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String getCpyEnvVar() {
		return cpyEnvVar;
	}

	public void setCpyEnvVar(String cpyEnvVar) {
		this.cpyEnvVar = cpyEnvVar;
	}

	public String getJobPriority() {
		return jobPriority;
	}

	public void setJobPriority(String jobPriority) {
		this.jobPriority = jobPriority;
	}

	public String getClass_path() {
		return class_path;
	}

	public void setClass_path(String class_path) {
		this.class_path = class_path;
	}

	public String ftpSite = null;

}