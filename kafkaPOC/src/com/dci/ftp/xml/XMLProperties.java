package com.dci.ftp.xml;
/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.xalan.xsltc.runtime.Node;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import com.dci.ftp.exception.XMLPropertiesException;
import com.dci.ftp.logger.DCILogger;

/**
 * @@author Durgesh
 * Created on Sep 3, 2007
 * This is XMLProperties class
  */
public class XMLProperties {
	
	  public static ClientBean getProperties(String clientId, String filePath) throws XMLPropertiesException    {

	  	XPathFactory factory =  XPathFactory.newInstance();
		XPath xPath = factory.newXPath();
		InputSource inputSource= null;
		
		NodeList clients = null;
		ClientBean clientBean = null;

		try{
			
			inputSource=new InputSource(new FileInputStream(filePath));
			clients = (NodeList)xPath.evaluate("/LoadDoc/client[@id='"+clientId+"']" ,inputSource, XPathConstants.NODESET);

						
			for (int i = 0; i <clients.getLength(); i++){
				
				clientBean= new ClientBean();						
		 		Element client = (Element)clients.item(i);
		 		
		 		clientBean.ftpSite = xPath.evaluate("ftpsite/text()",client);			
				clientBean.ftpId = xPath.evaluate("ftpid/text()",client);
				clientBean.ftpPass = xPath.evaluate("ftppass/text()",client);			
				clientBean.ifsPath = xPath.evaluate("IFSPath/text()",client);			
				clientBean.library = xPath.evaluate("library/text()",client);			
				clientBean.classpath = xPath.evaluate("classpath/text()",client);			
				clientBean.jobd = xPath.evaluate("jobd/text()",client);			
				clientBean.jobq = xPath.evaluate("jobq/text()",client);			
				clientBean.user = xPath.evaluate("user/text()",client);			
				clientBean.email = xPath.evaluate("com.dci.ftp.email/text()",client);			
				clientBean.watchFolder = xPath.evaluate("watchFolder/text()",client);			
				clientBean.absPath = xPath.evaluate("absPath/text()",client);		
				clientBean.dbClientId=xPath.evaluate("dbClientId/text()",client);
				clientBean.clientName=client.getAttribute("name");
				clientBean.client =xPath.evaluate("jiraClientName/text()",client);
				clientBean.class_path = xPath.evaluate("classpath/text()",client);
				clientBean.cpyEnvVar = xPath.evaluate("cpyEnvVar/text()",client);			
				clientBean.Job_Description = xPath.evaluate("jobd/text()",client);			
				clientBean.Job_Name = xPath.evaluate("jobn/text()",client);			
				clientBean.Job_Queue =xPath.evaluate("jobq/text()",client);			
				clientBean.jobPriority =xPath.evaluate("jobp/text()",client);			
				clientBean.log =xPath.evaluate("log/text()",client);			
				clientBean.password =xPath.evaluate("password/text()",client);			
				clientBean.userId =xPath.evaluate("userId/text()",client);			
				
				
			
			}
			
		}catch(XPathExpressionException xe){
			xe.printStackTrace();
			DCILogger.error(xe.getMessage());
			throw new XMLPropertiesException(xe.getMessage());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			DCILogger.debug("Please check the file path of 'docbuilder.xml'");
			DCILogger.error(e.getMessage());
			throw new XMLPropertiesException(e.getMessage());
		} catch(Exception e1){
			e1.printStackTrace();
			DCILogger.error(e1.getMessage());
			throw new XMLPropertiesException(e1.getMessage());
		}

		return clientBean;

	  }	
	  
	  public static BaseBean getBaseProperties(String clientId, String filePath) throws XMLPropertiesException    {

		  	XPathFactory factory =  XPathFactory.newInstance();
			XPath xPath = factory.newXPath();
			InputSource inputSource= null;
			
			NodeList beans = null;
			NodeList beans1 = null;
			BaseBean baseBean = null;

			try{
				
				inputSource=new InputSource(new FileInputStream(filePath));
				beans = (NodeList)xPath.evaluate("/properties/client[@id='"+clientId+"']" ,inputSource, XPathConstants.NODESET);
			
				for (int i = 0; i <beans.getLength(); i++){
				
					baseBean= new BaseBean();						
			 		Element bean = (Element)beans.item(i);
			 		//String value = xPath.evaluate("property[@name='class_path']/@value",bean);
			 		
			 	
			 			baseBean.client =xPath.evaluate("property[@name='jiraClientName']/@value",bean);
			 			baseBean.class_path = xPath.evaluate("property[@name='class_path']/@value",bean);
			 			baseBean.cpyEnvVar = xPath.evaluate("property[@name='cpyEnvVar']/@value",bean);			
			 			baseBean.Job_Description = xPath.evaluate("property[@name='Job_Description']/@value",bean);			
			 			baseBean.Job_Name = xPath.evaluate("property[@name='Job_Name']/@value",bean);			
			 			baseBean.Job_Queue =xPath.evaluate("property[@name='Job_Queue']/@value",bean);			
			 			baseBean.jobPriority =xPath.evaluate("property[@name='jobPriority']/@value",bean);			
			 			baseBean.log =xPath.evaluate("property[@name='log']/@value",bean);			
			 			baseBean.password =xPath.evaluate("property[@name='password']/@value",bean);			
			 			baseBean.user = xPath.evaluate("property[@name='user']/@value",bean);			
			 			
				
				}
				
			}catch(XPathExpressionException xe){
				xe.printStackTrace();
				DCILogger.error(xe.getMessage());
				throw new XMLPropertiesException(xe.getMessage());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				DCILogger.debug("Please check the file path of 'docbuilder.xml'");
				DCILogger.error(e.getMessage());
				throw new XMLPropertiesException(e.getMessage());
			} catch(Exception e1){
				e1.printStackTrace();
				DCILogger.error(e1.getMessage());
				throw new XMLPropertiesException(e1.getMessage());
			}

			return baseBean;

		  }	
}