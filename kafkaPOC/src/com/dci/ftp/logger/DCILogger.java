package com.dci.ftp.logger;

import java.io.FileReader;
import java.util.Properties;
/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * @@author Charles Created on Sep 11, 2007 This is DCILogger class
 */
public class DCILogger {

	public static Logger logger = Logger.getRootLogger();
	static ResourceBundle bundle = ResourceBundle.getBundle("UtilProperties");

	static String log4jConfigFile = bundle.getString("log4jConfigFile");
	static {

		DOMConfigurator.configure(System.getProperty("user.dir") + log4jConfigFile);
	}

	/**
	 * @@param info_str
	 */
	public static void debug(String info_str) {

		DCILogger.logger.info(info_str);

	}

	/**
	 * @@param warn_str
	 */
	public static void warn(String warn_str) {

		DCILogger.logger.warn(warn_str);
	}

	/**
	 * @@param error_str
	 */
	public static void error(String error_str) {

		DCILogger.logger.error(error_str);

	}

	/**
	 * @@param fatal_str
	 */
	public static void audit(String fatal_str) {

		DCILogger.logger.fatal(fatal_str);
	}

}
