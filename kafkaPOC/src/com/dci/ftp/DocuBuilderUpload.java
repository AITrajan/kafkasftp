package com.dci.ftp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.ResourceBundle;

import com.dci.ftp.logger.DCILogger;
import com.amazonaws.services.dynamodbv2.xspec.S;
import com.dci.ftp.as400.db.DBUpdate;
import com.dci.ftp.as400.utilities.ConnectAS400;
import com.dci.ftp.as400.utilities.MessageBean;

import com.dci.ftp.exception.FileMoveException;
import com.dci.ftp.exception.RemoveMessageException;
import com.dci.ftp.exception.StoredProcException;
import com.dci.ftp.exception.SubmitCMDException;
import com.dci.ftp.exception.XMLPropertiesException;
import com.dci.ftp.xml.BaseBean;
import com.dci.ftp.xml.ClientBean;
import com.dci.ftp.xml.XMLProperties;
import com.ibm.as400.access.AS400;
import com.dci.ftp.as400.utilities.*;

/**
 * @@author Durgesh Created on Sep 3, 2007 This is DocuBuilderUpload class
 * 
 */

public class DocuBuilderUpload {

	/**
	 * This is the main method which executes the following tasks. Read messages
	 * from Message Queue Move the file from ftp directory to ifs folder Execute
	 * stored procedure Execute submit java program
	 * 
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 * 
	 * @@param args
	 */
	public static void initiateFileTransfer(String ClientID, String fileName, String filePath)
			throws UnsupportedEncodingException, IOException {

		/*
		 * File logfile = new File("R:/s3/sftpdownload/logs/"); FileOutputStream foslg =
		 * null;
		 */

		/*
		 * if(!logfile.exists()) logfile.mkdirs(); foslg=new
		 * FileOutputStream(logfile+"/log.txt", true);
		 * foslg.write(("\n"+"    :    Inside the DocuBuilderUpload function").getBytes(
		 * "UTF-8"));
		 */
		DCILogger.audit("Initiating transfer...");
		AS400 system = null;
		ArrayList messages = null;
		try {

			/*
			 * Properties bundle = new Properties(); FileReader reader = new
			 * FileReader("cFolder/UtilProperties.properties");
			 */
			ResourceBundle bundle = ResourceBundle.getBundle("UtilProperties");
			// bundle.load(reader);
			String systemName = (String) bundle.getString("systemName");
			String password = (String) bundle.getString("password");
			String userID = (String) bundle.getString("userID");
			String timeToLive = (String) bundle.getString("maxTimeToLive");
			String sleepTimeString = (String) bundle.getString("sleepTime");
			String libraryName = (String) bundle.getString("libraryName");
			String objectName = (String) bundle.getString("objectName");
			String objectType = (String) bundle.getString("objectType");
			String awsTempPath = (String) bundle.getString("localStoragePath");
			System.out.println(libraryName);
			String toAddress = (String) bundle.getString("toAddress");
			String docuBuilderFile = System.getProperty("user.dir") + (String) bundle.getString("docubuilderPath");
			//String preconfigFile = System.getProperty("user.dir") + (String) bundle.getString("preconfigFile");
			docuBuilderFile = docuBuilderFile.replace("\\", "/");
		//	preconfigFile = preconfigFile.replace("\\", "/");

			long maxTimeToLive = new Long(timeToLive).longValue();
			long sleepTime = new Long(sleepTimeString).longValue();

			ConnectAS400 connectAS400 = new ConnectAS400();
			system = connectAS400.connect(systemName, userID, password);

			MessageBean messageBean = null;

			// iterate through the messages
			boolean isValid = false;
			int i = 0;

			messageBean = new MessageBean();
			isValid = connectAS400.isMessageValid(1000, maxTimeToLive);

			String[] arr = null;
			if (isValid) {
				DCILogger.audit("Message is valid");
				// load xml properties
				ClientBean clientBean = null;
				BaseBean baseBean = null;
				try {
					if (fileName.contains("/")) {
						arr = fileName.split("/");
						clientBean = XMLProperties.getProperties(arr[0], docuBuilderFile);
						// baseBean=XMLProperties.getBaseProperties(clientBean.dbClientId,
						// preconfigFile);
					} else {
						arr = new String[] { "", fileName };
						clientBean = XMLProperties.getProperties(ClientID, docuBuilderFile);
						// baseBean=XMLProperties.getBaseProperties(clientBean.dbClientId,
						// preconfigFile);
					}
					DCILogger.audit("Fetched data from docubuilder.xml file");
				} catch (XMLPropertiesException e2) {
					// Auto-generated catch block
					DCILogger.audit("Exception occured while fetching data from docubuilder.xml file");
					e2.printStackTrace();
					throw new Exception(e2.getMessage());
				}

//				if (connectAS400.isFileInWatchFolder(system, filePath.replace("//", "/").replace(arr[1], ""),
//						clientBean.watchFolder, clientBean.getAbsPath().replace("//", "/")+arr[0])) {
				if (clientBean != null) {
					String emailSubjectMessage = null;
					String emailMessage = null;
					boolean sendEmail = false;
					boolean submitCMDResult = false;
					boolean storedProcResult = false;
					String newIFSFilePath = "";
					// move and rename the file
					try {
						if (clientBean.ifsPath == null)
							throw new XMLPropertiesException(" Verify XML Properties. 'ifspath' element is null.");
						if (fileName != null) {
							newIFSFilePath = connectAS400.renameDir(system, filePath, clientBean.ifsPath,
									clientBean.watchFolder);
							System.out.println("New Path: " + newIFSFilePath);
							DCILogger.audit("File transfered to client location");
						}
						if (newIFSFilePath.equalsIgnoreCase("") || newIFSFilePath == null) {
							System.out.println("The new filepath is blank.");
							DCILogger.audit("File Move Failed");
							throw new FileMoveException(
									"File Move Failed. Skipping stored proc and submit command job.");
						} else {
							System.out.println("The newfilepath is: " + newIFSFilePath);
							DCILogger.audit("New file path is:" + newIFSFilePath);
						}
						// submit stored proc
						if (clientBean.dbClientId == null) {
							DCILogger.audit("Client ID is null");
							throw new XMLPropertiesException(" Verify XML Properties, 'clientId' element is null");

						}
						String[] temp = null;
						temp = newIFSFilePath.split(":");
						newIFSFilePath = "/datacom" + temp[1];
						/*storedProcResult = DBUpdate.callStoredProc(clientBean.library, clientBean.dbClientId,
								newIFSFilePath);*/
						DBUpdate.callStoredProc(clientBean.library, clientBean.dbClientId,
								newIFSFilePath);
						// foslg.write(("\n"+" : SP call made with return value
						// as:"+storedProcResult).getBytes("UTF-8"));
						//
					//	DCILogger.audit("Stored procedure returned: " + storedProcResult);
						System.out.println("The SP updated the table.");
						// uncomment the following block after unit test
/*						if (storedProcResult) {
							System.out.println("The SP updated the table.");
						} else {
							emailSubjectMessage = "DCIFTPUPLOAD:Failed submit job";
							emailMessage = "\nFailed to execute the stored procedure library : " + clientBean.library
									+ ", Client ID : " + clientBean.dbClientId + " IFSPath : " + clientBean.ifsPath;
							DCILogger.audit(emailSubjectMessage + emailMessage);

							sendEmail = true;
						}
*/
						/*
						 * submitCMDResult = connectAS400.submitAS400CommandJob(system,
						 * clientBean.dbClientId, clientBean.classpath, clientBean.jobq,
						 * clientBean.jobd, clientBean.user);
						 */// foslg.write(("\n"+" : AS400CommandJob returned with value:
							// "+submitCMDResult).getBytes("UTF-8"));
//							submitCMDResult = connectAS400.submitUploadjob(baseBean.user,baseBean.client,baseBean.Job_Name,baseBean.Job_Description,baseBean.Job_Queue,baseBean.userRes,baseBean.log,baseBean.cpyEnvVar,baseBean.jobPriority,baseBean.class_path);
						submitCMDResult = connectAS400.submitUploadJob(clientBean);
						// submitCMDResult = new CommandJob().submitUploadJob(clientBean);
						// new CommandJob().submitUploadJob(clientBean.userId, clientBean.dbClientId,
						// clientBean.Job_Name, clientBean.Job_Description, clientBean.Job_Queue,
						// clientBean.user, clientBean.log, clientBean.cpyEnvVar,
						// clientBean.jobPriority, clientBean.class_path,clientBean.password);
						if (submitCMDResult) {
							DCILogger.audit("DocuBuilderUpload : Successfully executed Submit Command Job");
							DCILogger.debug("DocuBuilderUpload : Successfully executed Submit Command Job");

						} else {
							throw new SubmitCMDException("Submit CMD Failed. ");
						}

					} catch (FileMoveException fme) {
						DCILogger.audit("File move exception occured:" + fme.getLocalizedMessage());

						fme.printStackTrace();
						emailSubjectMessage = "DCIFTPUPLOAD: Stored Procedure Failed";
						emailMessage = fme.getMessage() + "\nFailed to rename/move the file  " + messageBean.filePath
								+ ". Client ID :" + messageBean.cliendID;
						sendEmail = true;
						// send email
					} catch (StoredProcException spe) {
						DCILogger.audit("Stored procedure exception occured:" + spe.getLocalizedMessage());
						spe.printStackTrace();
						emailSubjectMessage = "DCIFTPUPLOAD:Failed stored proc";
						emailMessage = spe.getMessage() + "\nFailed to execute the stored procedure library : "
								+ clientBean.library + ", Client ID : " + clientBean.dbClientId + " IFSPath : "
								+ clientBean.ifsPath;
						sendEmail = true;
						// send email
					} catch (SubmitCMDException sce) {
						DCILogger.audit(" Exception occured while submitting command:" + sce.getLocalizedMessage());
						sce.printStackTrace();
						emailSubjectMessage = "DCIFTPUPLOAD:Failed submit job";
						emailMessage = sce.getMessage() + "\nFailed to execute submit command  " + messageBean.filePath
								+ ". Client ID :" + messageBean.cliendID;
						sendEmail = true;
						// send email
					} catch (RuntimeException re) {
						DCILogger.audit("Runtime exception occured:" + re.getLocalizedMessage());
						re.printStackTrace();
						emailSubjectMessage = "DCIFTPUPLOAD:Failed to execute FTP";
						emailMessage = re.getMessage();
						sendEmail = true;
					}
					if (sendEmail) {
						ArrayList recepients = new ArrayList();
						recepients.add(toAddress);

						try {
							boolean submitEmailResult = connectAS400.sendAS400Email(system, emailSubjectMessage,
									emailMessage, toAddress);
						} catch (Exception e) {
							DCILogger.audit("Exception occured:" + e.getLocalizedMessage());
							e.printStackTrace();

						}
					}
				}
//				}
			} else {
				DCILogger.audit("Message is invalid");
			}

//				connectAS400.removeMessage(system, messageBean.getMessageKey(),libraryName,objectName,objectType);

		} catch (Exception e) {
			DCILogger.audit("Exception occured :" + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
}
