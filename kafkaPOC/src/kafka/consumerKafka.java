package kafka;

import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpStatus;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.json.JSONException;
import org.json.JSONObject;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

import kafka.admin.AdminUtils;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import scala.collection.mutable.HashMap;

import java.awt.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import com.dci.ftp.*;
import com.dci.ftp.logger.DCILogger;;

public class consumerKafka {
	private final static String TOPIC = "SFTP";
	private static AmazonS3 s3client;
	public static final Logger developerLog = Logger.getLogger(consumerKafka.class.getSimpleName());
	private static String localStoragePath;
	private static String acessKey;
	private static String secretKey;

	private static Properties props = new Properties();
	private static Consumer<Long, String> consumer;

	private static Consumer<Long, String> createConsumer() throws IOException {

		FileReader reader = new FileReader("application.properties");
		props.load(reader);
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, props.getProperty("kafkaConsumer.endpointUrl"));
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "sftpgroupid");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		// producerprops.put("enable.idempotence", "true");
		// props.put("isolation.level", "read_committed");
		// props.put("enable.auto.commit", "false");
		// DCILogger.audit("Consumer configation recieved");
		final Consumer<Long, String> consumer = new KafkaConsumer<>(props);

		consumer.subscribe(Collections.singletonList(TOPIC));
		return consumer;
	}

	public static void runConsumer() throws InterruptedException, JSONException, IOException {

		/*
		 * final int giveUp = 1; int noRecordsCount = 0; boolean lflag = true;
		 */
		Properties prop = new Properties();
		FileReader reader = new FileReader("application.properties");
		prop.load(reader);

		// @SuppressWarnings("unused")
		// Logger logger = Logger.getRootLogger();
		// ResourceBundle prop = ResourceBundle.getBundle("Papplication");

		localStoragePath = prop.getProperty("localStoragePath");
//		localStoragePath=localStoragePath.replace("\\", "/");
		acessKey = prop.getProperty("amazonProperties.accessKey");
		secretKey = prop.getProperty("amazonProperties.secretKey");
		if (acessKey != null && secretKey != null) {
			DCILogger.audit("Recieved bucket credentials...");
		} else {
			DCILogger.audit(" Bucket credentials not recieved!!!!");
		}
		AWSCredentials credentials = new BasicAWSCredentials(acessKey, secretKey);
		s3client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(Regions.US_EAST_1).build();
		// DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		// Date date = new Date();

		final Consumer<Long, String> consumer = createConsumer();
		DCILogger.audit(" Consumer created successfully..");
		System.out.println("Consumer Created.");
		while (true) {
			@SuppressWarnings("deprecation")
			ConsumerRecords<Long, String> consumerRecords = consumer.poll(10000);
			consumerRecords.empty();
			System.out.println("Poll set for the Consumer instance.");
			String record = null;
			String bucketName = null;
			String fileName = null;

			if (consumerRecords.isEmpty())
				System.out.println("No records fetched by the consumer");

			if (!consumerRecords.isEmpty())
				for (ConsumerRecord<Long, String> records : consumerRecords) {
					DCILogger.audit("record recieved...");
					System.out.println("Processing record...");
					record = records.value();
					try {
						JSONObject jsonObj = new JSONObject(record);
						bucketName = jsonObj.get("bucketName").toString();
						fileName = jsonObj.get("objectName").toString();

						DCILogger.audit("bucketName recieved is:" + bucketName + "and file name is :" + fileName);
						System.out.println("Bucket Name is:" + bucketName + " And file name is:" + fileName);
					} catch (Exception e) {
						DCILogger.audit("Exception occured is: " + e.getLocalizedMessage());
					}

					MDC.put("category", "com.dci.kafka.service.consumer");

					try {

						DCILogger.audit("Enter into com.dci.kafka.service.consumer || Method Name : runConsumer() ||"
								+ bucketName + " fileName:" + fileName);

						S3Object s3object;
						/*
						 * ListObjectsRequest listObjectsRequest = new
						 * ListObjectsRequest().withBucketName(bucketName)
						 * .withPrefix("americas").withDelimiter("/");
						 * System.out.println(listObjectsRequest);
						 */
						String newfileName = "";
						if (fileName.contains("!l@mbd@"))
							newfileName = fileName.replace("!l@mbd@", "/").trim();
						else
							newfileName = fileName;

						if (s3client.doesObjectExist(bucketName, newfileName)) {
							s3object = s3client.getObject(new GetObjectRequest(bucketName, newfileName));
//						s3client.doesObjectExist(bucketName, fileName);
							if (s3object == null) {
								DCILogger.audit("s3Object is null : || " + s3object);
							}

							S3ObjectInputStream inputStream = s3object.getObjectContent();
//						File file = null;
							String appendPath = "";
							String bclientID = "";
							String filename = "";
							String propClientID = "";
							String[] arr = null;
							if (bucketName != null) {
								if (bucketName.contains("jhfsftp")) {
									appendPath = "/johnhancock";
									propClientID = bucketName;
								} else if (bucketName.contains("lmsftp")) {
									appendPath = "/leggmason";
								}
								else if(bucketName.contains("gsamsftp")) {
									appendPath = "/gsam";
									propClientID = bucketName;
								}
								else if(bucketName.contains("gssftp")) {
									appendPath = "/gs";
									propClientID = bucketName;
								}
								else if(bucketName.contains("manulifesftp")) {
									appendPath = "/manulife";
									propClientID = bucketName;
								}
								if (fileName.contains("!l@mbd@")) {
									arr = fileName.split("!l@mbd@");
									if (arr.length > 1) {
//									bclientID = bucketName;
										int i = 0;
										for (String a : arr) {
											if (i < arr.length - 1) {
												bclientID = bclientID + "/" + a;
											}
											i++;
										}
										filename = arr[arr.length - 1];
										propClientID = arr[0];
									}
									appendPath = appendPath + bclientID;
								} else {
									filename = newfileName;
								}

							}

							DCILogger.audit("Folder created  || " + localStoragePath + appendPath + "/" + filename);
							FileUtils.copyInputStreamToFile(inputStream,
									new File(localStoragePath + appendPath + "/" + filename));
//								FileUtils.copyInputStreamToFile(inputStream, file);
							DCILogger.audit(
									"file downloaded to  -> || " + localStoragePath + appendPath + "/" + filename);

							String finalPath = localStoragePath+appendPath+"/"+filename;
							if (prop.getProperty("deleteS3File").equalsIgnoreCase("true")) {
								if (arr != null) {
									if (arr.length > 1 || !newfileName.contains("/")) {
										s3client.deleteObject(new DeleteObjectRequest(bucketName, newfileName));

										DCILogger.audit("File transfered via sftp from bucket, is now deleted from S3");
										System.out.println(
												"File transfered via sftp from bucket, is now deleted from S3");
										DocuBuilderUpload.initiateFileTransfer(propClientID, newfileName, finalPath);
									}
								} else {
									if (!newfileName.contains("/")) {
										s3client.deleteObject(new DeleteObjectRequest(bucketName, newfileName));

										DCILogger.audit("File transfered via sftp from bucket, is now deleted from S3");
										System.out.println(
												"File transfered via sftp from bucket, is now deleted from S3");
										DocuBuilderUpload.initiateFileTransfer(propClientID, newfileName, finalPath);
									}
									DCILogger.audit("Event received on folder creation, thus no deletion is done.");
								}

							} else {
								DCILogger.audit("File deleting rule is set to false");
							}
						}

					} catch (Exception e) {
						new AmazonServiceException(
								"Error into com.dci.kafka.service.consumer || Method Name : runConsumer() ||" + e, e);

						DCILogger.audit(
								"Error into com.dci.kafka.service.consumer || Method Name : runConsumer() ||" + e);

					}

				}

		}

	}

	public static void main(String... args) throws Exception {
		BasicConfigurator.configure(); // initializing log4j
		runConsumer();
//		DocuBuilderUpload.initiateFileTransfer("jhfsftp.docubuilder");
	}

}
